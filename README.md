# miditomusic - Extract music objects from MIDI data

## Requirements

- [Python 3.6](www.python.org)
- [Mido](https://mido.readthedocs.io/en/latest/) 