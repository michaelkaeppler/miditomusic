import unittest
from logging import Logger, WARNING
from mido import Message
from miditomusic.parsers import ParserInterface, ParsingError


class AbstractParser(ParserInterface):
    listening_events = []

    def __init__(self, is_strict):
        super().__init__()
        self._is_strict = is_strict

    @property
    def is_strict(self):
        return self._is_strict

    def process_event(self, event, globals):
        pass


class AbstractParserTest(unittest.TestCase):
    def setUp(self):
        self.parser_strict = AbstractParser(is_strict=True)
        self.parser_not_strict = AbstractParser(is_strict=False)

    def test_parser_name(self):
        parser_name = str(self.parser_not_strict)
        parser_name_ref = "AbstractParser"
        self.assertEqual(parser_name, parser_name_ref)

    def test_has_logger(self):
        parser_logger = self.parser_not_strict.logger
        self.assertIsInstance(parser_logger, Logger)

    def test_strict_mode(self):
        event = Message('note_on')
        description = "Unexpected event"
        with self.assertRaises(ParsingError) as cm:
            self.parser_strict.log_or_error(description, event)

        exception_msg = str(cm.exception)
        self.assertIn(description, exception_msg)

    def test_not_strict_mode(self):
        event = Message('note_on')
        description = "Unexpected event"
        logger = self.parser_not_strict.logger
        with self.assertLogs(logger, level=WARNING):
            self.parser_not_strict.log_or_error(description, event)


if __name__ == '__main__':
    unittest.main()
