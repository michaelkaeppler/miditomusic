import unittest
from mido import Message
from miditomusic.parsers import NoteParser, ParsingError
from miditomusic.musicelements import Note, Rest


class EndActivePitchTest(unittest.TestCase):
    def test_end_active_pitch(self):
        pitch = 65
        velocity = 64
        event_delta_time = 32
        start_time = 192
        end_time = 384
        duration = end_time - start_time
        output_note_ref = Note(pitch, duration, velocity)

        parser = NoteParser()
        active_event = Message('note_on', note=pitch, velocity=velocity, time=event_delta_time)
        parser._active_pitches[active_event.note] = (active_event, start_time)
        output_note = parser._output_note(pitch, end_time)

        self.assertEqual(output_note, output_note_ref)


class ProcessEventBaseTests(unittest.TestCase):
    pitch = 60
    velocity = 80
    current_times = [0, 128, 320]
    globals = [{'current_time': current_time} for current_time in current_times]

    def setUp(self):
        self.parser = NoteParser(is_strict=True)

    def test_note_on(self):
        event = Message('note_on', note=self.pitch)
        active_pitches_ref = {self.pitch: (event, self.current_times[0])}
        self.parser.globals = self.globals[0]
        output_note = self.parser.process_event(event)

        self.assertIsNone(output_note)
        self.assertEqual(self.parser._active_pitches, active_pitches_ref)

    def test_note_on_already_on(self):
        event_one = Message('note_on', note=self.pitch)
        event_two = event_one.copy()
        self.parser.globals = self.globals[1]
        self.parser.process_event(event_one)

        self.parser.globals = self.globals[2]
        with self.assertRaises(ParsingError):
            self.parser.process_event(event_two)

    def test_note_on_and_off(self):
        duration = self.current_times[2] - self.current_times[1]
        event_one = Message('note_on', note=self.pitch, velocity=self.velocity)
        event_two = Message('note_off', note=self.pitch)
        output_note_ref = Note(self.pitch, duration, self.velocity)

        self.parser.globals = self.globals[1]
        self.parser.process_event(event_one)

        self.parser.globals = self.globals[2]
        output_note = self.parser.process_event(event_two)

        self.assertEqual(output_note, output_note_ref)

    def test_note_on_and_off_by_velocity(self):
        duration = self.current_times[2] - self.current_times[1]
        event_one = Message('note_on', note=self.pitch, velocity=self.velocity)
        event_two = Message('note_on', note=self.pitch, velocity=0)
        output_note_ref = Note(self.pitch, duration, self.velocity)

        self.parser.globals = self.globals[1]
        self.parser.process_event(event_one)

        self.parser.globals = self.globals[2]
        output_note = self.parser.process_event(event_two)

        self.assertEqual(output_note, output_note_ref)

    def test_note_off_but_not_on(self):
        event = Message('note_off', note=self.pitch, velocity=self.velocity)

        self.parser.globals = self.globals[2]
        with self.assertRaises(ParsingError):
            self.parser.process_event(event)

    def test_start_with_rest(self):
        event = Message('note_on', note=self.pitch)
        duration = self.current_times[1]
        output_note_ref = Rest(duration)

        self.parser.globals = self.globals[1]
        output_note = self.parser.process_event(event)

        self.assertEqual(output_note, output_note_ref)


class ProcessEventComplexTests(unittest.TestCase):
    def setUp(self):
        self.parser = NoteParser()

    def _process_events(self, events):
        output_notes = []
        globals = {'current_time': 0}
        self.parser.globals = globals

        for event in events:
            globals['current_time'] += event.time
            output_note = self.parser.process_event(event)
            if output_note:
                output_notes.append(output_note)

        return output_notes

    def test_simple_on_off_one(self):
        test_track = [Message('note_on', note=62, velocity=85, time=0),
                      Message('note_off', note=62, velocity=0, time=192),
                      Message('note_on', note=48, velocity=52, time=0),
                      Message('note_off', note=48, velocity=0, time=256)]

        output_notes_ref = [Note(pitch=62, duration=192, velocity=85),
                            Note(pitch=48, duration=256, velocity=52)]

        output_notes = self._process_events(test_track)

        self.assertListEqual(output_notes, output_notes_ref)

    def test_simple_on_off_two(self):
        test_track = [Message('note_on', note=53, velocity=42, time=0),
                      Message('note_on', note=53, velocity=0, time=37),
                      Message('note_on', note=49, velocity=73, time=0),
                      Message('note_on', note=49, velocity=0, time=56)]

        output_notes_ref = [Note(pitch=53, duration=37, velocity=42),
                            Note(pitch=49, duration=56, velocity=73)]

        output_notes = self._process_events(test_track)

        self.assertListEqual(output_notes, output_notes_ref)

    def test_intertwined_events(self):
        test_track = [Message('note_on', note=59, velocity=62, time=0),
                      Message('note_on', note=62, velocity=48, time=20),
                      Message('note_off', note=59, velocity=55, time=50),
                      Message('note_on', note=55, velocity=65, time=0),
                      Message('note_off', note=62, velocity=0, time=70),
                      Message('note_off', note=55, velocity=0, time=35)]

        output_notes_ref = [Note(pitch=59, duration=70, velocity=62),
                            Note(pitch=62, duration=120, velocity=48),
                            Note(pitch=55, duration=105, velocity=65)]

        output_notes = self._process_events(test_track)

        self.assertListEqual(output_notes, output_notes_ref)

    def test_rests(self):
        test_track = [Message('note_on', note=68, velocity=50, time=0),
                      Message('note_off', note=68, velocity=0, time=70),
                      Message('note_on', note=68, velocity=50, time=30),
                      Message('note_off', note=68, velocity=0, time=50)]

        output_notes_ref = [Note(pitch=68, duration=70, velocity=50),
                            Rest(duration=30),
                            Note(pitch=68, duration=50, velocity=50)]

        output_notes = self._process_events(test_track)

        self.assertListEqual(output_notes, output_notes_ref)


class FinalizationTests(unittest.TestCase):
    pitches = [58, 62]
    velocity = 67
    current_times = [0, 110, 350]
    globals = [{'current_time': current_time} for current_time in current_times]

    def setUp(self):
        self.parser = NoteParser(is_strict=False)

    def test_finalization_not_clean(self):
        events = [Message('note_on', note=pitch, velocity=self.velocity) for pitch in self.pitches]
        output_notes_ref = [Note(pitch=self.pitches[0], duration=350, velocity=self.velocity),
                            Note(pitch=self.pitches[1], duration=240, velocity=self.velocity)]

        for i in range(2):
            self.parser.globals = self.globals[i]
            self.parser.process_event(events[i])

        self.parser.globals = self.globals[2]

        output_notes = self.parser.finalize()
        self.assertListEqual(output_notes, output_notes_ref)

    def test_finalization_clean(self):
        events = [Message('note_on', note=self.pitches[0], velocity=self.velocity),
                  Message('note_off', note=self.pitches[0], velocity=0)]

        for i in range(2):
            self.parser.globals = self.globals[i]
            self.parser.process_event(events[i])

        self.parser.globals = self.globals[2]

        output_notes = self.parser.finalize()
        self.assertFalse(output_notes)


if __name__ == '__main__':
    unittest.main()
