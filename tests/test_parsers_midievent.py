import unittest
from unittest.mock import Mock, call
from itertools import accumulate
from mido import Message
from miditomusic.parsers import MIDIEventParser


class RegisterParserTests(unittest.TestCase):
    def setUp(self):
        self.mocked_note_parser_one = Mock()
        self.mocked_note_parser_two = Mock()
        self.mocked_note_parser_one.listening_events = ['note_on', 'note_off']
        self.mocked_note_parser_two.listening_events = ['note_on', 'program_change']
        self.event_parser = MIDIEventParser(event_source=[])

    def test_some_events_one_parser(self):
        event_types_to_parser_map_ref = {'note_on': [self.mocked_note_parser_one],
                                         'note_off': [self.mocked_note_parser_one]}
        self.event_parser._register_parser(self.mocked_note_parser_one)
        event_types_to_parser_map = self.event_parser._event_types_to_parser_map

        self.assertDictEqual(event_types_to_parser_map, event_types_to_parser_map_ref)

    def test_some_events_some_parsers(self):
        event_types_to_parser_map_ref = {'note_on': [self.mocked_note_parser_one, self.mocked_note_parser_two],
                                         'note_off': [self.mocked_note_parser_one],
                                         'program_change': [self.mocked_note_parser_two]}
        self.event_parser._register_parser(self.mocked_note_parser_one)
        self.event_parser._register_parser(self.mocked_note_parser_two)
        event_types_to_parser_map = self.event_parser._event_types_to_parser_map

        self.assertDictEqual(event_types_to_parser_map, event_types_to_parser_map_ref)


class ProcessEventTests(unittest.TestCase):
    def test_make_timestep(self):
        # events should increase the parser's `_current_time`, regardless
        # of whether there is a suitable child parser

        delta_times = [0, 192, 32, 128]
        current_times_ref = list(accumulate(delta_times))
        test_track = [Message('note_on', time=delta_times[0]),
                      Message('note_off', time=delta_times[1]),
                      # the following event will be discarded
                      Message('program_change', time=delta_times[2]),
                      Message('note_on', time=delta_times[3])]

        event_parser = MIDIEventParser(event_source=[])
        mocked_note_parser = Mock()
        mocked_note_parser.listening_events = ['note_on', 'note_off']
        event_parser._register_parser(mocked_note_parser)

        current_times = []
        for event in test_track:
            event_parser._process_event(event)
            current_times.append(event_parser.globals['current_time'])

        self.assertListEqual(current_times, current_times_ref)

    def test_forward_events(self):
        test_track = [Message('note_on'),
                      Message('note_off'),
                      Message('control_change')]

        event_parser = MIDIEventParser(event_source=[])

        mocked_parsers = [Mock() for i in range(3)]
        mocked_parsers_listening_events = [['note_on', 'note_off'],
                                           ['note_on', 'control_change'],
                                           ['note_off', 'control_change']]

        for parser, events in zip(mocked_parsers, mocked_parsers_listening_events):
            parser.listening_events = events
            event_parser._register_parser(parser)

        for event in test_track:
            event_parser._process_event(event)

        mocked_parsers_calls_ref = [[call(test_track[0]), call(test_track[1])],
                                    [call(test_track[0]), call(test_track[2])],
                                    [call(test_track[1]), call(test_track[2])]]

        for parser, calls_ref in zip(mocked_parsers, mocked_parsers_calls_ref):
            parser.process_event.assert_has_calls(calls_ref)


if __name__ == '__main__':
    unittest.main()
