import logging
from abc import ABC, abstractmethod
from typing import Any, Dict, Iterable, Iterator, List, Optional, Union
from mido import Message
from miditomusic.musicelements import Note, Rest


class ParsingError(Exception):
    def __init__(self, description: str, event: Any) -> None:
        message = description
        if event:
            message = "While parsing event {}: ".format(repr(event)) + message

        Exception.__init__(self, message)


def get_silent_logger(name: str) -> logging.Logger:
    logger = logging.getLogger(name)
    # Disable logging by default
    logger.addHandler(logging.NullHandler())
    return logger


class ParserInterface(ABC):
    def __init__(self) -> None:
        self.logger = get_silent_logger(repr(self))
        self.globals: Dict[str, Any] = {}

    def __str__(self) -> str:
        return self.__class__.__name__

    def log_or_error(self, description: str, event: Any = None) -> None:
        if self.is_strict:
            raise ParsingError(description, event)
        else:
            self.logger.warning(description)

    @property
    @classmethod
    @abstractmethod
    def listening_events(cls) -> List[str]:
        raise NotImplementedError

    @property
    @abstractmethod
    def is_strict(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def process_event(self, event: Message) -> Any:
        raise NotImplementedError


class MIDIEventParser:
    def __init__(self,
                 event_source: Iterable[Message],
                 parsers: Optional[List[ParserInterface]] = None) -> None:

        self.logger = get_silent_logger(repr(self))
        self._event_source = event_source
        self._event_types_to_parser_map: Dict[str, List[ParserInterface]] = {}
        self.globals = {'current_time': 0}
        if parsers:
            for parser in parsers:
                self._register_parser(parser)

    def _register_parser(self, parser: ParserInterface) -> None:
        listening_events = parser.listening_events
        self.logger.info("Registering parser '%s' that listens to events '%s'", parser, listening_events)
        for event in listening_events:
            if event not in self._event_types_to_parser_map:
                self._event_types_to_parser_map[event] = [parser]
            else:
                self._event_types_to_parser_map[event].append(parser)
        parser.globals = self.globals

    def _process_event(self, event: Message) -> List[Any]:
        output_events = []
        self.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        self.logger.debug("Current time: %d", self.globals['current_time'])
        self.logger.debug("Processing event '%s'", event)

        self.globals['current_time'] += event.time

        if event.type in self._event_types_to_parser_map:
            responsible_parsers = self._event_types_to_parser_map[event.type]
            for parser in responsible_parsers:
                self.logger.debug("Feeding event '%s' to parser '%s'", event, parser)
                output_event = parser.process_event(event)
                if output_event:
                    output_events.append(output_event)
        else:
            self.logger.debug("Found no matching parser for event type '%s', ignoring", event.type)

        if output_events:
            self.logger.debug("Outputting parsed events %s", output_events)

        return output_events

    def __iter__(self) -> Iterator[Any]:
        for event in self._event_source:
            output_events = self._process_event(event)
            if output_events:
                # `yield from` is used to yield the events one by one
                # instead of the whole list at once
                yield from output_events


class NoteParser(ParserInterface):
    listening_events = ['note_on', 'note_off']

    def __init__(self, is_strict: bool = False):
        super().__init__()
        self._is_strict = is_strict
        self._active_pitches: Dict[int, Message] = {}
        self._last_output_time = 0

    @property
    def is_strict(self) -> bool:
        return self._is_strict

    def _output_note(self, pitch: int, current_time: int) -> Note:
        active_event, start_time = self._active_pitches[pitch]
        duration = current_time - start_time
        self.logger.debug("Ending pitch %d", pitch)
        del self._active_pitches[pitch]
        self._last_output_time = current_time
        return Note(pitch, duration, active_event.velocity)

    def _output_rest(self, current_time: int) -> Rest:
        duration = current_time - self._last_output_time
        return Rest(duration)

    def finalize(self) -> List[Note]:
        pending_notes = []
        if self._active_pitches:
            pending_pitches = list(self._active_pitches.keys())
            self.log_or_error("Noticed pending pitches {} during parser finalization".format(pending_pitches))
            # Won't be reached in strict mode
            pending_notes = [self._output_note(pitch, self.globals['current_time']) for pitch in pending_pitches]
        return pending_notes

    def process_event(self, event: Message) -> Union[Note, Rest, None]:
        current_time = self.globals['current_time']
        output_note_or_rest: Union[Note, Rest, None] = None
        if event.note in self._active_pitches:
            self.logger.debug("Pitch %d is already active", event.note)
            # 'note_on' with velocity = 0 is equivalent to a 'note_ofF' event
            if event.type == 'note_off' or event.velocity == 0:
                self.logger.debug("Received '%s' with velocity %d", event.type, event.velocity)
                output_note_or_rest = self._output_note(event.note, current_time)
            else:
                self.log_or_error("Velocity > 0, ignoring event '{}'".format(event), event)
        elif event.type == 'note_on':
            if not self._active_pitches and current_time != self._last_output_time:
                output_note_or_rest = self._output_rest(current_time)

            self.logger.debug("Adding pitch %d at timestep %d to list of active pitches", event.note, current_time)
            self._active_pitches[event.note] = (event, current_time)

        elif event.type == 'note_off':
            self.log_or_error("Cannot end pitch {}, it was not active".format(event.note), event)

        return output_note_or_rest
