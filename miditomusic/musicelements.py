class Note:
    def __init__(self, pitch: int, duration: int, velocity: int) -> None:
        self.pitch = pitch
        self.duration = duration
        self.velocity = velocity

    def __repr__(self) -> str:
        return 'Note(pitch {}, duration {}, velocity {})'.format(self.pitch, self.duration, self.velocity)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Note):
            return NotImplemented
        return self.pitch == other.pitch and self.duration == other.duration and self.velocity == other.velocity


class Rest:
    def __init__(self, duration: int) -> None:
        self.duration = duration

    def __repr__(self) -> str:
        return 'Rest(duration {})'.format(self.duration)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Rest):
            return NotImplemented
        return self.duration == other.duration
